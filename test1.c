#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "struct.h"
#include "vmescript_parser.h"
#include "vmescript_setup_parse.h"
#include "vmescript_util.h"

extern int vmescript_yylineno;
vmescript_command * command_list;
vmescript_variable * variable_list;

void vmescript_yyerror (char const *s)
{
  fprintf (stderr, "%s line: %d\n", s, vmescript_yylineno);
  exit(1);
}

int main (int argc, char * argv[])
{
  /* yylloc.first_line = yylloc.last_line = 1; */
  /* yylloc.first_column = yylloc.last_column = 0; */

  command_list = NULL;
  variable_list = NULL;

  char * op[] = {"eq", "neq", "lt", "lte", "gt", "gte"};

  if(argc < 2) {
    fprintf(stderr, "Missing file name.\n");
    exit(1);
  }


  vmescript_number num;
  num._is_intg = 1;
  num._intg = 1;
  vmescript_set_variable("sys_irq", num);
  vmescript_set_variable("mesy_mcst", num);
  vmescript_set_variable("mesy_readout_num_events", num);
  vmescript_set_variable("mesy_eoe_marker", num);
  vmescript_set_variable("Bus_Number", num);

  for (int i = 1; i < argc; i++){
    printf("processing %s\n", argv[i]);
    if (vmescript_parse_file(argv[i]) == 1){
      fprintf(stderr, "something went wrong\n");
      exit(1);
    }
  }
 
  /* if (vmescript_parse_file(argv[1]) == 1){ */
  /*   fprintf(stderr, "something went wrong\n"); */
  /*   exit(1); */
  /* } */

  if (command_list != NULL){
    do {
      switch (command_list->_type){
      case ct_read:
	printf("read a%d d%d 0x%x\n",
	       command_list->_address_mode,
	       command_list->_data_width,
	       command_list->_address);
	break;
      case ct_write:
	printf("write a%d d%d 0x%x 0x%x\n",
	       command_list->_address_mode,
	       command_list->_data_width,
	       command_list->_address,
	       command_list->_value);
	break;
      case ct_writeabs:
	printf("writeabs a%d d%d 0x%x 0x%x\n",
	       command_list->_address_mode,
	       command_list->_data_width,
	       command_list->_address,
	       command_list->_value);
	break;
      case ct_wait:
	printf("wait %dns\n",
	       command_list->_delay_ns);
	break;
      case ct_print:
	printf("print \"%s\"\n",
	       command_list->_string);
	break;
      case ct_accu_maskandrotate:
	printf("accu_mask_rotate 0x%x %d\n",
	       command_list->_accu_mask,
	       command_list->_accu_rotate);
	break;
      case ct_accu_test:
	printf("accu_test %s 0x%x \"%s\"\n",
	       op[command_list->_accu_test_op],
	       command_list->_accu_test_value,
	       command_list->_string);
	break;
      case ct_write_float_word:
	printf("write_float_word a%d d%d 0x%x %f\n",
	       command_list->_address_mode,
	       command_list->_address,
	       command_list->_value,
	       command_list->_dbl_value);
	break;
      }
      command_list = command_list->_next;
    }
    while (command_list != NULL);
  }

  return 0;
}
