#ifndef __VMESCRIPT_UTIL_HH__
#define __VMESCRIPT_UTIL_HH__

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include "struct.h"
#include "vmescript_setup_parse.h"
#include "vmescript_parser.h"

extern int vmescript_yylineno;
extern vmescript_command * command_list;
extern vmescript_variable * variable_list;

extern const char *_vmescript_lexer_filename;
extern int _vmescript_lexer_first_lineno;

extern FILE *_vmescript_lexer_read_fid;

extern const char *_vmescript_lexer_buf_ptr;
extern size_t _vmescript_lexer_buf_len;

void vmescript_yyerror (char const *);

bool vmescript_string_is_integer(const char *str);
int vmescript_parse_file(const char *filename);
int vmescript_parse_expr(const char *expr);
void vmescript_set_variable(char * name, vmescript_number num);
bool vmescript_get_variable_value(char * name, vmescript_variable * list, char ** value_str);
size_t vmescript_lexer_read(char* buf,size_t max_size);
void vmescript_set_vmescript_number_value(vmescript_variable * v, vmescript_number num);
vmescript_command* vmescript_alloc_cmd(int ct);
uint32_t vmescript_get_uint32_t(vmescript_number num);
void vmescript_math_op(vmescript_number * result, const vmescript_number * n1, char op, const vmescript_number * n3);
bool vmescript_to_unput(char * text, vmescript_variable * list, char ** to_unput, int base);

bool vmescript_stack_command_equals(const vmescript_stack_command *a, const vmescript_stack_command *b);
bool vmescript_stack_command_not_equals(const vmescript_stack_command *a, const vmescript_stack_command *b);
bool vmescript_stack_command_is_valid(const vmescript_stack_command *cmd);

#endif/*__VMESCRIPT_UTIL_HH__*/
