%{
  #include <math.h>
  #include <stdio.h>
  #include <stdint.h>
  #include <stdlib.h>
  #include <stdbool.h>
  #include <string.h>
  #include "struct.h"
  #include "vmescript_util.h"
  #include "vmescript_setup_parse.h"


  int vmescript_yylex (void);

  char const *vmescript_yypath;
  extern int vmescript_yylineno;
  
  extern vmescript_command * command_list;
  extern vmescript_variable * variable_list;

%}

%union {
  vmescript_number num;
  uint32_t u32;
  uint64_t u64;
  uint8_t addr_mode;
  uint8_t width;
  double dbl;
  char *str;
  vmescript_command * cmd;
  
}

/* Bison declarations. */

%define parse.error detailed
%define api.prefix {vmescript_yy}

%token <num>       TK_NUM
%token <num>       TK_DOUBLE
%token <addr_mode> TK_ADDRESS_WIDTH
%token <width>     TK_DATA_WIDTH
%token <u32>       TK_HEX
%token <u32>       TK_BINARY
%token <str>       TK_STRING
%token <str>       TK_IDENTIFIER
%token <u32>       TK_WAITSPEC
%token <u32>       TK_PART

%left '-' '+'
%left '*' '/'
%precedence NEG   /* negation--unary minus */
%right '^'        /* exponentiation */

%token TK_MBLTFIFO
%token TK_WAIT
%token TK_WRITE
%token TK_PRINT
%token TK_READ
%token TK_MASK_ROTATE
%token TK_ACCU_TEST
%token TK_WRITEABS
%token TK_BLTFIFO
%token TK_SET
%token TK_MBLTS
%token TK_MVLC_STACK_BEGIN
%token TK_MVLC_READ_TO_ACCU
%token TK_MVLC_SHIFT_ACCU
%token TK_MVLC_SIGNAL_ACCU
%token TK_WRITESPECIAL
%token TK_STACK_END
%token TK_SETBASE
%token TK_WRITE_FLOAT_WORD

%token TK_SLOW

%token TK_NEWLINE

%token TK_OP_EQ;
%token TK_OP_NEQ;
%token TK_OP_LESS;
%token TK_OP_LESSEQ;
%token TK_OP_GREATER;
%token TK_OP_GREATEREQ;

%type <num> expr
%type <u32> int_expr
%type <num> calc
%type <u32> addr_expr
%type <u32> comp

%type <cmd> write
%type <cmd> print
%type <cmd> read
%type <cmd> wait
%type <cmd> mbltfifo
%type <cmd> stmt
%type <cmd> stmt_list
%type <cmd> bltfifo
%type <cmd> accu_mask_rotate
%type <cmd> accu_test
%type <cmd> writeabs
%type <cmd> setbase
%type <cmd> write_float_word

%% /* The grammar follows. */

program:
          any_nl stmt_list any_nl        {
	    if (command_list == NULL)
	      command_list = $2;
	    else {
	      vmescript_command *c;
	      for (c = command_list; c->_next != NULL ; c = c->_next);
	      c->_next = $2;
	    }
	  }
        | any_nl { }
        ;

stmt_list:
          stmt              { $$ = $1;           }
        | stmt_list many_nl stmt    {
	    vmescript_command *c;
	    if ($1 == NULL) {
	      $$ = $3;
	    }
	    else { 
	      $$ = $1;
	      for (c = $$; c->_next != NULL ; c = c->_next);
	      c->_next = $3;
	    }
				 
	  }

many_nl: '\n' { }
        | many_nl '\n' { }
        ;

any_nl: /* note: empty */
        | many_nl { }
        ;

stmt:
          write             { $$ = $1;           }
        | print             { $$ = $1;           }
        | read              { $$ = $1;           }
        | wait              { $$ = $1;           }
        | mbltfifo          { $$ = $1;           }
        | bltfifo           { $$ = $1;           }
        | accu_mask_rotate  { $$ = $1;           }
        | accu_test         { $$ = $1;           }
        | writeabs          { $$ = $1;           }
        | setbase           { $$ = $1;           }
        | set               { $$ = NULL;         }
        | write_float_word  { $$ = $1;           }
;

calc:
          TK_NUM             { $$ = $1;       }
        | TK_DOUBLE          { $$ = $1;       }
        | TK_HEX             { $$._intg = $1;
                               $$._is_intg = true;
                             }
        | TK_BINARY          { $$._intg = $1;
                               $$._is_intg = true;
                             }
        | calc '+' calc      {
            vmescript_math_op(&$$, &$1, '+', &$3);
          }
        | calc '-' calc      {
            vmescript_math_op(&$$, &$1, '-', &$3);
          }
        | calc '*' calc      {
            vmescript_math_op(&$$, &$1, '*', &$3);
          }
        | calc '/' calc      {
            vmescript_math_op(&$$, &$1, '/', &$3);
          }
        | '-' calc   %prec NEG        {
	    vmescript_number zero;
            zero._is_intg = true;
            zero._intg = 0;
            vmescript_math_op(&$$, &zero, '-', &$2);
          }
        | '(' calc ')'       { $$ = $2;           }
        ;


expr:
          calc               { $$ = $1;  }
        | '$' '(' expr ')'   { $$ = $3;           }
        ;

int_expr:
          expr               { $$ = vmescript_get_uint32_t($1); }
        ;

addr_expr:
          TK_HEX             { $$ = $1;           }
        | TK_NUM             { $$ = $1._intg;      }
        | TK_BINARY          { $$ = $1;            }
        | '(' calc ')'       { $$ = vmescript_get_uint32_t($2);  }
        | '$' '(' calc ')'   { $$ = vmescript_get_uint32_t($3);  }
        ;



comp:
          TK_OP_EQ           { $$ = 0; }
        | TK_OP_NEQ          { $$ = 1; }
        | TK_OP_LESS         { $$ = 2; }
        | TK_OP_LESSEQ       { $$ = 3; }
        | TK_OP_GREATER      { $$ = 4; }
        | TK_OP_GREATEREQ    { $$ = 5; }
        ;

write:
          TK_WRITE TK_ADDRESS_WIDTH TK_DATA_WIDTH addr_expr int_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_write);
	    c->_address_mode     = $2;
	    c->_data_width       = $3;
	    c->_address          = $4;
	    c->_value            = $5;
	    $$                   = c;

	  }
|          TK_HEX int_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_write);
	    c->_address           = $1;
	    c->_value             = $2;
	    c->_address_mode      = 32;
	    c->_data_width        = 16;
	    $$                    = c;
	  }

print:
          TK_PRINT TK_STRING {
	    vmescript_command * c = vmescript_alloc_cmd(ct_print);
	    c->_string            = $2;
	    $$                    = c;
	  }

read:
          TK_READ TK_ADDRESS_WIDTH TK_DATA_WIDTH addr_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_read);
	    c->_address_mode      = $2;
	    c->_data_width        = $3;
	    c->_address           = $4;
	    $$                    = c;
	  }

mbltfifo:
          TK_MBLTFIFO TK_ADDRESS_WIDTH addr_expr int_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_mbltfifo);
	    c->_address_mode      = $2;
	    c->_address           = $3;
	    c->_value             = $4;
	    $$                    = c;
	  }


wait:
          TK_WAIT TK_WAITSPEC {
	    vmescript_command * c = vmescript_alloc_cmd(ct_wait);
	    c->_delay_ns          = $2;
	    $$                    = c;
	  }
| TK_WAIT int_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_wait);
	    c->_delay_ns          = $2;
	    $$                    = c;
	  }

accu_mask_rotate:
          TK_MASK_ROTATE addr_expr int_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_accu_maskandrotate);
	    c->_accu_mask         = $2;
	    c->_accu_rotate       = $3;
	    $$                    = c;
	  }

accu_test:
          TK_ACCU_TEST comp addr_expr TK_STRING {
	    vmescript_command * c = vmescript_alloc_cmd(ct_accu_test);
	    c->_accu_test_op      = $2;
	    c->_accu_test_value   = $3;
	    c->_string            = $4;
	    $$                    = c;
	  }

writeabs:
          TK_WRITEABS TK_ADDRESS_WIDTH TK_DATA_WIDTH addr_expr int_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_writeabs);
	    c->_address_mode      = $2;
	    c->_data_width        = $3;
	    c->_address           = $4;
	    c->_value             = $5;
	    $$                    = c;
	  }

bltfifo:
          TK_BLTFIFO TK_ADDRESS_WIDTH addr_expr int_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_bltfifo);
	    c->_address_mode      = $2;
	    c->_address           = $3;
	    c->_value             = $4;
	    $$                    = c;
	  }

setbase:
          TK_SETBASE addr_expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_setbase);
	    c->_address           = $2;
	    $$                    = c;
	  }

set:
          TK_SET TK_IDENTIFIER expr {
	    vmescript_set_variable($2, $3);
	  }
| TK_SET TK_IDENTIFIER TK_WAITSPEC {
	    vmescript_number num;
	    num._is_intg = 1;
	    num._intg = $3;
	    vmescript_set_variable($2, num);
	  }

write_float_word:
          TK_WRITE_FLOAT_WORD TK_ADDRESS_WIDTH addr_expr TK_PART expr {
	    vmescript_command * c = vmescript_alloc_cmd(ct_write_float_word);
	    c->_address_mode      = $2;
	    c->_address           = $3;
	    c->_value             = $4;
	    c->_dbl_value         = $5._dbl;
	    $$                    = c;
	  }
|        TK_WRITE_FLOAT_WORD TK_ADDRESS_WIDTH addr_expr TK_NUM expr {
	    if (vmescript_get_uint32_t($4) > 1)
	      vmescript_yyerror("Invalid part (upper / 1 or lower / 0 required for write_float_word).\n");
	    vmescript_command * c = vmescript_alloc_cmd(ct_write_float_word);
	    c->_address_mode      = $2;
	    c->_address           = $3;
	    c->_value             = $4._intg;
	    c->_dbl_value         = $5._dbl;
	    $$                    = c;
	  }

%%
