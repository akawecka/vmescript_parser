#include "vmescript_util.h"

const char *_vmescript_lexer_filename = NULL;
int _vmescript_lexer_first_lineno = 0;

FILE *_vmescript_lexer_read_fid = NULL;

const char *_vmescript_lexer_buf_ptr = NULL;
size_t _vmescript_lexer_buf_len = 0;

bool vmescript_string_is_integer(const char *str) {
  while (isspace(*str))
    str++;

  if (*str == '+' || *str == '-')
    str++;


  while (*str) {
    if (!isdigit(*str))
      return false;
    str++;
  }

  return true;
}

int vmescript_parse_file(const char *filename)
{
  int ret;
  _vmescript_lexer_filename = filename;
  _vmescript_lexer_first_lineno = vmescript_yylineno;

  _vmescript_lexer_read_fid = fopen(_vmescript_lexer_filename,"r");

  if (_vmescript_lexer_read_fid == NULL)
    {
      char *errstr = strdup(strerror(errno));
      fprintf(stderr, "Cannot open vmescript file '%s': %s.",
		     filename,errstr);
      exit(1);
    }

  ret = vmescript_yyparse();

  fclose(_vmescript_lexer_read_fid);

  _vmescript_lexer_filename = NULL;
  _vmescript_lexer_first_lineno = -1;
  _vmescript_lexer_read_fid = NULL;

  return ret;
}

int vmescript_parse_expr(const char *expr)
{
  int ret;
  _vmescript_lexer_filename = "string";
  _vmescript_lexer_first_lineno = vmescript_yylineno;
  _vmescript_lexer_buf_ptr = expr;
  _vmescript_lexer_buf_len = strlen(expr);

  ret = vmescript_yyparse();

  _vmescript_lexer_filename = NULL;
  _vmescript_lexer_first_lineno = -1;
  _vmescript_lexer_buf_ptr = NULL;
  _vmescript_lexer_buf_len = 0;

  return ret;
}

void vmescript_set_variable(char * name, vmescript_number num){
  bool matched = 0;
  vmescript_variable * t;
  for (t=variable_list; t != NULL; t = t->_next) {
    if (strcmp(t->_name, name) == 0) {
      vmescript_set_vmescript_number_value(t, num);
      matched = true;
      break;
    }
  }
  if (!matched) {
    vmescript_variable * tmp = malloc(sizeof(vmescript_variable));
    tmp->_name = strdup(name);
    vmescript_set_vmescript_number_value(tmp, num);

    if (variable_list == NULL) {
      tmp->_next = NULL;
      tmp->_prev = NULL;
      variable_list = tmp;
    }
    else {
      tmp->_next = variable_list;
      tmp->_prev = NULL;
      variable_list->_prev = tmp;
      variable_list = tmp;
    }
  }

}

bool vmescript_get_variable_value(char * name, vmescript_variable * list, char ** value_str){
  char tmp_str[100];
  if (value_str == NULL) {
    fprintf(stderr, "Memory allocation failed\n");
    return false;
  }

  ssize_t i = 0;
  bool success = false;
  int num_chars_written = 0;

  if (name == NULL)
    return false;
  if (list == NULL)
    return false;

  for ( ; list!= NULL; list = list->_next){

    if (strcmp(list->_name, name) == 0) {
      if (list->_value._is_intg)
	num_chars_written = snprintf(tmp_str,
				     sizeof(tmp_str),
				     "%d", list->_value._intg);
      else
	num_chars_written = snprintf(tmp_str,
				     sizeof(tmp_str),
				     "%.20g", list->_value._dbl);
        * value_str = strdup(tmp_str);
      if (*value_str == NULL) {
	fprintf(stderr, "Memory allocation failed\n");
	return false;
      }

      success = true;
      break;
    }
  }

  return success;

}

size_t vmescript_lexer_read(char* buf,size_t max_size)
{
  ssize_t n;

  if (_vmescript_lexer_buf_ptr)
    {
      if (max_size > _vmescript_lexer_buf_len)
	max_size = _vmescript_lexer_buf_len;

      memcpy(buf,_vmescript_lexer_buf_ptr,max_size);

      _vmescript_lexer_buf_ptr += max_size;
      _vmescript_lexer_buf_len -= max_size;

      return max_size;
    }

  n = (ssize_t) fread(buf,1,max_size,_vmescript_lexer_read_fid);

  if (n == 0)
    {
      if (feof(_vmescript_lexer_read_fid))
	return 0;
      fprintf(stderr,"Failure reading from configuration file.\n");
      return 0;
    }

  return (size_t) n;
}

void vmescript_set_vmescript_number_value(vmescript_variable * v, vmescript_number num){
  v->_value = num;
}

vmescript_command* vmescript_alloc_cmd(int ct){
  vmescript_command * c = malloc(sizeof(vmescript_command));
  if (c == NULL) {
    vmescript_yyerror("Memory allocation error.\n");
  }
    
  c->_type = ct;
  c->_address_mode = 0;
  c->_data_width = 0;
  c->_address = 0;
  c->_value = 0;
  c->_transfers = 0;
  c->_delay_ns = 0;
  c->_count_mask = 0;
  c->_accu_test_op = 0;
  c->_accu_test_value = 0;
  c->_accu_mask = 0;
  c->_accu_rotate = 0;
  c->_string = '\0';
  c->_next = NULL;
  c->_line_number = vmescript_yylineno;
}

vmescript_stack_command* vmescript_alloc_stack_cmd(int ct){
  vmescript_stack_command * c = malloc(sizeof(vmescript_stack_command));
  if (c == NULL) {
    vmescript_yyerror("Memory allocation error.\n");
  }

  c->type = ct;
  c->address = 0;
  c->value = 0;
  c->amod = 0;
  c->dataWidth = 0;
  c->transfers = 0;
  c->rate = 0;
  c->customValues = NULL;
  c->customValuesSize = 0;
  c->lateRead = false;
}

uint32_t vmescript_get_uint32_t(vmescript_number num){
  if (num._is_intg) {
    if (num._intg <= UINT32_MAX)
      return num._intg;
    else
      vmescript_yyerror("Invalid conversion (integer outside of range for uint32_t).\n");
  }
  else {
    if (abs(num._dbl) <= UINT32_MAX){
      uint32_t tmp = (uint32_t) num._dbl;
      if (tmp != num._dbl)
	fprintf(stderr, "input file line number %d: Double (%f) converted with loss to integer (%d).\n", vmescript_yylineno, num._dbl, tmp);
      return tmp;
    }
    else
      vmescript_yyerror("Invalid conversion (double outside of range).\n");
  }
}

void vmescript_math_op(vmescript_number * result, const vmescript_number * n1, char op, const vmescript_number * n3){
  if (n1->_is_intg && n3->_is_intg) {
    result->_is_intg = true;
    result->_intg = n1->_intg;

    if(op == '+')
      result->_intg += n3->_intg;
    if(op == '-')
      result->_intg -= n3->_intg;
    if(op == '*')
      result->_intg *= n3->_intg;
    if(op == '/')
      result->_intg /= n3->_intg;
  }
  else {
    result->_is_intg = false;
    result->_dbl = (double) (n1->_is_intg ? n1->_intg : n1->_dbl);
    double tmp2 = (n3->_is_intg ? n3->_intg : n3->_dbl);
    if(op == '+')
      result->_dbl += tmp2;
    if(op == '-')
      result->_dbl -= tmp2;
    if(op == '*')
      result->_dbl *= tmp2;
    if(op == '/')
      result->_dbl /= tmp2;
  }
}

bool vmescript_to_unput(char * text, vmescript_variable * list, char ** to_unput, int base){
  char * entire_text, * var_text, * pre_var_text;
  char return_text[100];
  entire_text = strdup(text);
  var_text = strstr(entire_text, "${");
  var_text = strndup(var_text + 2, strlen(var_text) - 3);
  pre_var_text = strndup(entire_text, strlen(entire_text) - strlen(var_text) - 3);
  
  char * base_name;
  bool worked = false;

  if (base == 1)
    base_name = "binary";
  else if (base == 15)
    base_name = "hexadecimal";
  else
    vmescript_yyerror("Unknown base.");

  bool success = false;
  char * value_str;
  success = vmescript_get_variable_value(var_text, list, &value_str);

  if (success){
    if (vmescript_string_is_integer(value_str)) {
      int value_int = atoi(value_str);
      if (value_int > base){
	char * err_msg = malloc(strlen(var_text) + 128);
	sprintf(err_msg, "Value larger than %s passed to %s number variable replacement.", var_text, base_name);
	vmescript_yyerror(err_msg);
	free(err_msg);
      }
      else {
	strcpy(return_text, pre_var_text);
	if (base == 1){
	  strcat(return_text, value_str);
	}
	else if (base == 15){
	  char value_str_hex[128];
	  sprintf(value_str_hex, "%x", value_int);
	  strcat(return_text, value_str_hex);
	}
	* to_unput = strdup(return_text);
	worked = true;
      }
    }
    else {
      char * err_msg = malloc(strlen(base_name) + 128);
      sprintf(err_msg, "Cannot convert to %s number.", base_name);
      vmescript_yyerror(err_msg);
      free(err_msg);
    }
  }
  else {
    char * err_msg = malloc(strlen(var_text) + 128);
    sprintf(err_msg, "Undeclared variable was used (%s).", var_text);
    vmescript_yyerror(err_msg);
    free(err_msg);
  }

  return success;
}

bool vmescript_stack_command_equals(const vmescript_stack_command *a, const vmescript_stack_command *b) {
  if (a->type != b->type ||
      a->address != b->address ||
      a->value != b->value ||
      a->amod != b->amod ||
      a->dataWidth != b->dataWidth ||
      a->transfers != b->transfers ||
      a->rate != b->rate ||
      a->lateRead != b->lateRead) {
    return false;
  }

  if (a->customValuesSize != b->customValuesSize) {
    return false;
  }

  return memcmp(a->customValues, b->customValues, a->customValuesSize * sizeof(uint32_t)) == 0;
}

bool vmescript_stack_command_not_equals(const vmescript_stack_command *a, const vmescript_stack_command *b) {
  return !vmescript_stack_command_equals(a, b);
}

bool vmescript_stack_command_is_valid(const vmescript_stack_command *cmd) {
  return cmd->type != COMMAND_TYPE_INVALID;
}
