/*
 * {declarations}
 * %%
 * {rules}
 * %%
* {subroutines}
 * \n                      return TK_NEWLINE;
 */

%{
#include "struct.h"
#include "vmescript_parser.h"
#include "vmescript_setup_parse.h"
#include "vmescript_util.h"
#include <ctype.h>

size_t vmescript_lexer_read(char* buf,size_t max_size);

#define YY_INPUT(buf,result,max_size) \
{ \
  result = vmescript_lexer_read(buf,max_size); \
  if (!result) result = YY_NULL; \
}

extern vmescript_variable * variable_list;

void vmescript_yyerror(char const *);


%}


%x C_COMMENT
%x PRINT
%x EXPECT_EOF

%option noyywrap
%option yylineno
%option prefix="vmescript_yy"

%%

mbltfifo                return TK_MBLTFIFO;
wait                    return TK_WAIT;
write                   return TK_WRITE;

read                    return TK_READ;
accu_mask_rotate        return TK_MASK_ROTATE;
accu_test               return TK_ACCU_TEST;
writeabs                return TK_WRITEABS;
bltfifo                 return TK_BLTFIFO;
set                     return TK_SET;
mblts                   return TK_MBLTS;
mvlc_stack_begin        return TK_MVLC_STACK_BEGIN;
mvlc_read_to_accu       return TK_MVLC_READ_TO_ACCU;
mvlc_mask_shift_accu    return TK_MVLC_SHIFT_ACCU;
mvlc_signal_accu        return TK_MVLC_SIGNAL_ACCU;
mvlc_writespecial       return TK_WRITESPECIAL;
mvlc_stack_end          return TK_STACK_END;
setbase                 return TK_SETBASE;
write_float_word        return TK_WRITE_FLOAT_WORD;


slow                    return TK_SLOW;

(==|eq)     return TK_OP_EQ;
(!=|neq)    return TK_OP_NEQ;
(\<|lt)     return TK_OP_LESS;
(\<=|lte)   return TK_OP_LESSEQ;
(\>gt)      return TK_OP_GREATER;
(\>=|gte)   return TK_OP_GREATEREQ;

upper                  { vmescript_yylval.u32 = 1;
                         return TK_PART; }

lower                  { vmescript_yylval.u32 = 0;
                         return TK_PART; }

"/*"                   { BEGIN(C_COMMENT); }
<C_COMMENT>"*/"        { BEGIN(INITIAL); }
<C_COMMENT>.           { }

"print"[ \t]+          { BEGIN(PRINT);
                         return TK_PRINT;
                       }
print"\n"              { BEGIN(PRINT);
                        unput('\n');
                        return TK_PRINT;
                       }
<PRINT>.*$             { 
                         vmescript_yylval.str = strdup(vmescript_yytext);
                         BEGIN(INITIAL);
                         return TK_STRING;
                       }
 /* this rule catches the case of a print statement without a trailing new line. */
<PRINT>.*              { 
                         vmescript_yylval.str = strdup(vmescript_yytext);
                         BEGIN(EXPECT_EOF);
                         return TK_STRING;
                       }

<EXPECT_EOF><<EOF>>    { return 0; }          /* not really needed */

[-+()=/*\n]            { return *vmescript_yytext; }
"$"                    { return *vmescript_yytext; }


[aA]32                 {
                           vmescript_yylval.addr_mode = 32;
                           return TK_ADDRESS_WIDTH;
                       }

[aA]24                 {
                           vmescript_yylval.addr_mode = 24;
                           return TK_ADDRESS_WIDTH;
                       }

[aA]16                 {
                           vmescript_yylval.addr_mode = 16;
                           return TK_ADDRESS_WIDTH;
                       }

[dD]32                 {
                           vmescript_yylval.width = 32;
                           return TK_DATA_WIDTH;
                       }

[dD]16                 {
                           vmescript_yylval.width = 16;
                           return TK_DATA_WIDTH;
                       }

[0-9]+(s|ms|ns)        {
                           char *end;					
                           vmescript_yylval.u64 = strtoll(vmescript_yytext, &end, 10);
                           if (strcmp(end,"s") == 0) {			
                               vmescript_yylval.u64 = vmescript_yylval.u64 * 1000000000;
                           }						
                           else if (strcmp(end,"ms") == 0) {			
                               vmescript_yylval.u64 = vmescript_yylval.u64 * 1000000;
                           }
                           else if (strcmp(end,"ns") == 0) {
                           }

                           return TK_WAITSPEC;				
                        }						
									

[0-9]+                  {
                           char *end;
                           vmescript_yylval.num._is_intg = true;
                           vmescript_yylval.num._intg = strtoll(vmescript_yytext, &end, 10);
	                   if (*end != '\0') {
		               vmescript_yyerror("Invalid integer.");
	                   }
	                   return TK_NUM;
                        }

(([0-9]*\.[0-9]+|[0-9]+\.)([eE][-+]?[0-9]+)?|[0-9]+[eE][-+]?[0-9]+) {
	                   char *end;
                           vmescript_yylval.num._is_intg = false;
	                   vmescript_yylval.num._dbl = strtod(vmescript_yytext, &end);
	                   if (*end != '\0') {
	                   	vmescript_yyerror("Invalid double.");
                    	   }
	                   return TK_DOUBLE;
                        }


"0"[xX][0-9a-fA-F]+     {
                           char *end;
                           vmescript_yylval.u32 = strtoll(vmescript_yytext, &end, 0);
	                   if (*end != '\0' ) {
		               vmescript_yyerror("Invalid hex.");
	                   }
	                   return TK_HEX;
                        }

"0"[bB][01]+            {
                           char *end;
                           vmescript_yylval.u32 = strtoll(vmescript_yytext+2, &end, 2);
                           if (*end != '\0' ) {
		               vmescript_yyerror("Invalid binary.");
	                   }
	                   return TK_BINARY;

                        }

"0"[bB][01]*"${"[a-zA-Z_][a-zA-Z_0-9]+"}" {
  char * to_unput = malloc(50);
  bool success = vmescript_to_unput(vmescript_yytext, variable_list, &to_unput, 1);

  if (success)
    for (ssize_t i = strlen(to_unput) - 1; i >= 0; i--)
      unput(to_unput[i]);
  else
    vmescript_yyerror("Invalid conversion of variable");
  free(to_unput);

}

"0"[xX][0-9a-fA-F]*"${"[a-zA-Z_][a-zA-Z_0-9]+"}" {
  char * to_unput = malloc(50);
  bool success = vmescript_to_unput(vmescript_yytext, variable_list, &to_unput, 15);

  if (success)
    for (ssize_t j = strlen(to_unput) - 1; j >= 0; j--)
      unput(to_unput[j]);
  else
    vmescript_yyerror("Invalid conversion of variable");

  free(to_unput);

}

"${"[a-zA-Z_][a-zA-Z_0-9]+"}" {
  char * tmp;
  tmp = strndup(vmescript_yytext + 2, strlen(vmescript_yytext) - 3);
  bool success = false;
  char * value_str;
  success = vmescript_get_variable_value(tmp, variable_list, &value_str);
  ssize_t i = 0;

  if (success) {
    for (i = strlen(value_str) - 1; i >= 0; i--)
      unput(value_str[i]);
  }
  else {
    char * err_msg = malloc(strlen(tmp) + 128);
    sprintf(err_msg, "Undeclared variable was used (%s).", tmp);
    vmescript_yyerror(err_msg);
    free(err_msg);
  }
  free(value_str);

}

\"[^\"\n]*\"            {
                           char *p;
                           vmescript_yylval.str = strdup(vmescript_yytext + 1);
                           p = strrchr(vmescript_yylval.str, '"');
                           if (!p) {
                               vmescript_yyerror("Internal error, missing '\"'!");
                           }
                           *p = '\0';
                           return TK_STRING;
                        }

[a-zA-Z_][a-zA-Z_0-9]*  {
                           vmescript_yylval.str = strdup(vmescript_yytext);
                           return TK_IDENTIFIER;
}

[ \t\n]                 ;

#.*                     ;

.                       vmescript_yyerror("Unexpected symbol.");

%%

