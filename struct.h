#ifndef __STRUCT__H__
#define __STRUCT__H__

#include <stdbool.h>
#include <stdint.h>

// Copied from mesytec-mvlc

// Communication with the MVLC is done using 32-bit wide binary data words.
// Results from commands and stack executions are also 32-bit aligned.
// All data is in little-endian byte order.

// Super commands are commands that are directly interpreted and executed
// by the MVLC.
// The values in the SuperCommands enum contain the 2 high bytes of the
// command word.
// The output of super commands always goes to pipe 0, the CommandPipe.

#define SUPER_CMD_MASK      0xFFFF
#define SUPER_CMD_SHIFT     16
#define SUPER_CMD_ARG_MASK  0xFFFF
#define SUPER_CMD_ARG_SHIFT 0

typedef uint16_t vmescript_super_command_type;

enum
  {
    SUPER_CMD_BUFFER_START = 0xF100, // Marks the beginning of a command buffer
    SUPER_CMD_BUFFER_END   = 0xF200, // Marks the end of a command buffer
    SUPER_REFERENCE_WORD   = 0x0101, // A reference word to be mirrored by the MVLC
    SUPER_READ_LOCAL       = 0x0102, // Read a MVLC register
    SUPER_READ_LOCAL_BLOCK = 0x0103, // Read a block of local memory (not implemented).
    SUPER_WRITE_LOCAL      = 0x0204, // Write a MVLC register
    SUPER_WRITE_RESET      = 0x0206, // Reset command (TODO: is this even used?)
    SUPER_ETH_DELAY        = 0x0207  // Ethernet-specific delay command for the throttle port only.
    // This command is not embedded in CmdBufferStart/End.
    // The lowest 16 bits contain a delay value in µs.
    // The max value ((2^16)-1) is used to completely stop the MVLC
    // from sending data packets.
  };

typedef uint8_t vmescript_stack_command_type;

// Stack-only commands. These can be written into the stack memory area
// starting from StackMemoryBegin using WriteLocal commands.
//
// The output produced by a stack execution can go to either the
// CommandPipe or the DataPipe. This is encoded in the StackStart command.

#define STACK_CMD_MASK      0xFF
#define STACK_CMD_SHIFT     24
#define STACK_CMD_ARG0_MASK 0x00FF
#define STACK_CMD_ARG0_SHIFT 16
#define STACK_CMD_ARG1_MASK 0x0000FFFF
#define STACK_CMD_ARG1_SHIFT 0
// 'late' flag for VME read and read_to_accu stored in bit 3 of the VME
// data width argument
#define STACK_LATE_READ_SHIFT 2

// Enum for StackCommandType
enum {
  STACK_CMD_STACK_START         = 0xF3, // First word in a command stack.
  STACK_CMD_STACK_END           = 0xF4, // Last word in a command stack
  STACK_CMD_VME_READ            = 0x12, // VME read requests including block reads.
  // Always FIFO mode reads (no address increment) for block transfers
  // since FW0036. Sensitive to the (stack scoped) SetAddressIncMode
  // in earlier firmware versions.

  STACK_CMD_VME_READ_SWAPPED    = 0x13, // For MBLT and 2eSST block reads: swaps word order. Use this instead
  // of VMERead if your module data arrives in the wrong order.
  // Always FIFO mode since FW0036 like VMERead above.

  STACK_CMD_VME_READ_MEM        = 0x32, // Same as VMERead but for reads from memory where the read address
  // needs to be incremented. New in FW0036.
  STACK_CMD_VME_READ_MEM_SWAPPED = 0x33,// Word swapped version of VMEReadMem. New in FW0036.

  STACK_CMD_VME_WRITE           = 0x23, // VME write requests.
  STACK_CMD_WRITE_MARKER        = 0xC2, // Writes a 32-bit marker value into the output data stream.
  STACK_CMD_WRITE_SPECIAL       = 0xC1, // Write a special value into the output data stream.
  // Values: 0=timestamp, 1=accumulator

  STACK_CMD_WAIT                = 0xC4, // Delay in units of MVLC clocks. The number of clocks to delay is
  // specified as a 24-bit number.

  STACK_CMD_SIGNAL_ACCU         = 0xC6, // Constant data word used to activate the internal signal array. This creates an
  // MVLC-internal IRQ signal.
  STACK_CMD_MASK_SHIFT_ACCU     = 0xC5, // First mask is applied, then the left rotation.
  STACK_CMD_SET_ACCU            = 0xC8, // Set the accumulator to a specific 32-bit value.
  STACK_CMD_READ_TO_ACCU        = 0x14, // Single register VME read into the accumulator.
  STACK_CMD_COMPARE_LOOP_ACCU   = 0xC7  // CompareMode, 0=eq, 1=lt, 2=gt. Loops to previous command if false.
};

typedef uint8_t vmescript_frame_types;

enum {
  FRAME_TYPE_SUPER_FRAME         = 0xF1, // Outermost command buffer response frame.
  FRAME_TYPE_SUPER_CONTINUATION  = 0xF2, // Continuation frame for Super Command mirror responses.
  // Same mechanism as 0xF3/0xF9 for the stack command layer.
  FRAME_TYPE_STACK_FRAME         = 0xF3, // Outermost frame for readout data produced by command stack execution.
  FRAME_TYPE_BLOCK_READ          = 0xF5, // Inner frame for block reads. Always contained within a StackFrame.
  FRAME_TYPE_STACK_ERROR         = 0xF7, // Error notification frame embedded either between readout data or
  // sent to the command port for monitoring.
  FRAME_TYPE_STACK_CONTINUATION  = 0xF9, // Continuation frame for StackFrame frames with the Continue bit set.
  // The last F9 frame in a sequence has the Continue bit cleared.
  FRAME_TYPE_SYSTEM_EVENT        = 0xFA  // Software generated frames used for transporting additional
  // information. See the system_event namespace for details.
};

// Header fields: Type[7:0], Continue[0:0], ErrorFlags[2:0], StackNum[3:0],
// CtrlId[2:0], Length[12:0]
// TTTT TTTT CEEE SSSS IIIL LLLL LLLL LLLL
// The Continue bit and the ErrorFlags are combined into a 4-bit FrameFlags field.
// Bitfield masks and shifts
#define FRAME_TYPE_SHIFT           24
#define FRAME_TYPE_MASK            0xFF
#define FRAME_FLAGS_MASK           0xF
#define FRAME_FLAGS_SHIFT          20
#define STACK_NUM_SHIFT            16
#define STACK_NUM_MASK             0xF
#define CTRL_ID_SHIFT              13
#define CTRL_ID_MASK               0x7  // 0b111
#define LENGTH_SHIFT               0
#define LENGTH_MASK                0x1FFF

typedef uint8_t vmescript_command_type;

enum {
  COMMAND_TYPE_INVALID = 0x0u,

  COMMAND_TYPE_STACK_START = 0xF3,
  COMMAND_TYPE_STACK_END = 0xF4,
  COMMAND_TYPE_VME_WRITE = 0x23,
  COMMAND_TYPE_VME_READ = 0x12,
  COMMAND_TYPE_VME_READ_SWAPPED = 0x13,
  COMMAND_TYPE_VME_READ_MEM = 0x32,
  COMMAND_TYPE_VME_READ_MEM_SWAPPED = 0x33,
  COMMAND_TYPE_WRITE_MARKER = 0xC2,
  COMMAND_TYPE_WRITE_SPECIAL = 0xC1,
  COMMAND_TYPE_WAIT = 0xC4,
  COMMAND_TYPE_SIGNAL_ACCU = 0xC6,
  COMMAND_TYPE_MASK_SHIFT_ACCU = 0xC5,
  COMMAND_TYPE_SET_ACCU = 0xC8,
  COMMAND_TYPE_READ_TO_ACCU = 0x14,
  COMMAND_TYPE_COMPARE_LOOP_ACCU = 0xC7,

  COMMAND_TYPE_SOFTWARE_DELAY = 0xEDu,
  COMMAND_TYPE_CUSTOM = 0xEEu
};

typedef enum {
  VME_DATA_WIDTH_D16,
  VME_DATA_WIDTH_D32,
} VMEDataWidth;

typedef enum {
  BLK2E_SST_RATE_LOW,
  BLK2E_SST_RATE_HIGH,
} Blk2eSSTRate;

typedef struct vmescript_stack_command_t vmescript_stack_command;

struct vmescript_stack_command_t{
  vmescript_command_type type;
  uint32_t address;
  uint32_t value;
  uint8_t amod;
  VMEDataWidth dataWidth;
  uint16_t transfers;  // max number of transfers for block read commands / number of produced data words for custom commands
  Blk2eSSTRate rate;
  uint32_t *customValues; // Pointer to array for custom values
  size_t customValuesSize; // Size of the custom values array
  bool lateRead;
};

enum vmescript_command_type
{
    ct_invalid,

    /* VME reads and writes. */
    ct_read,			/* 1 */
    ct_readabs,			/* 2 */
    ct_write,			/* 3 */
    ct_writeabs,		/* 4 */

    /* Delay when directly executing a script. */
    ct_wait,			/* 5 */

    /* Marker word to be inserted into the data stream by the controller. */
    ct_marker,			/* 6 */

    /* VME block transfers */
    ct_blt,			/* 7 */
    ct_bltfifo,			/* 8 */
    ct_mblt,			/* 9 */
    ct_mbltfifo,		/* 10 */
    ct_mbltswapped,		/* 11 */
    ct_mbltswappedfifo,		/* 12 */

    /* fast source synchronous block transfers */
    ct_blk2esst64,		/* 13 */
    ct_blk2esst64fifo,		/* 14 */
    ct_blk2esst64swapped,	/* 15 */
    ct_blk2esst64swappedfifo,	/* 16 */

    /* Meta commands to temporarily use a different base address for the */
    /* following commands and then reset back to the default base address. */
    ct_setbase,			/* 17 */
    ct_resetbase,		/* 18 */

    /* Low-level VMUSB specific register write and read commands. */
    ct_vmusb_writeregister,	/* 19 */
    ct_vmusb_readregister,	/* 20 */

    /* Low-level MVLC instruction to insert a special word into the data */
    /* stream. Currently 'timestamp' and 'stack_triggers' are implemented. The */
    /* special word code can also be given as a numeric value. */
    /* The type of the special word is stored in Command.value. */
    ct_mvlc_writespecial,	/* 21 */

    /* A meta block enclosed in meta_block_begin and meta_block_end. */
    ct_metablock,		/* 22 */

    /* Meta command to set a variable value. The variable is inserted into the */
    /* first (most local) symbol table given to the parser. */
    ct_setvariable,		/* 23 */

    /* Prints its arguments to the log output on a separate line. Arguments are */
    /* separated by a space by default which means string quoting is not */
    /* strictly required. */
    ct_print,			/* 24 */

    ct_mvlc_custom,		/* 25 */
    ct_mvlc_inlinestack,	/* 26 */
    ct_mvlc_wait,		/* 27 */
    ct_mvlc_signalaccu,		/* 28 */
    ct_mvlc_maskshiftaccu,	/* 29 */
    ct_mvlc_setaccu,		/* 30 */
    ct_mvlc_readtoaccu,		/* 31 */
    ct_mvlc_compareloopaccu,	/* 32 */

    /* 32 bit accumulator value containing the result of the last non-block vme */
    /* read operation. */
    ct_accu_set,	     // Set the accu to a constant value.
    ct_accu_maskandrotate,     // Mask, then left rotate the accu value.
    ct_accu_test,              // Compare against constant value and error out on failure.

    /* Meta command checking the mvme version against a supplied version number. */
    /* The check succeeds if the running mvme version is >= the supplied version. */
    /* MvmeRequireVersion */

    ct_write_float_word
};

enum AccuTestOp
{
    EQ,
    NEQ,
    LT,
    LTE,
    GT,
    GTE,
};

typedef struct vmescript_number_t vmescript_number;

struct vmescript_number_t
{
  bool _is_intg;
  int64_t _intg;
  double _dbl;
};

typedef struct vmescript_variable_t vmescript_variable;

struct vmescript_variable_t
{
  char * _name;
  vmescript_number _value;
  vmescript_variable * _next;
  vmescript_variable * _prev;
};

typedef struct vmescript_command_t vmescript_command;

struct vmescript_command_t
{
    enum vmescript_command_type _type;
    uint8_t _address_mode;
    uint8_t _data_width;
    uint32_t _address;
    uint32_t _value;
    uint32_t _transfers;
    uint32_t _delay_ns;
    uint32_t _count_mask;
    double _dbl_value;
    /* u8 blockAddressMode = vme_address_modes::A32; */
    /* uint32_t blockAddress = 0; */
    /* Blk2eSSTRate blk2eSSTRate = Blk2eSSTRate::Rate160MB; */

    /* QString warning; */
    /* s32 lineNumber = 0; */

    /* MetaBlock metaBlock = {}; */
    /* QStringList printArgs; */
    /* std::vector<u32> mvlcCustomStack; */
    /* // vme script parsed from a mvlc_stack_begin block */
    /* std::vector<std::shared_ptr<Command>> mvlcInlineStack; */

    /* // 'late' flag for read, readabs and mvlc_read_to_accu. */
    /* bool mvlcSlowRead = false; */

    /* // 'mem' mode if false, 'fifo' mode otherwise. Only used for the 'read' and */
    /* // 'readabs' commands to control the address increments for fake block reads */
    /* // resulting from MVLC stack accumulator use. */
    /* bool mvlcFifoMode = true; */

    /* // for the internal accu_test function */
    enum AccuTestOp _accu_test_op;
    uint32_t _accu_test_value;
    /* QString accuTestMessage; */
    uint32_t _accu_mask;
    uint32_t _accu_rotate;
    /* QString stringData; */
    char * _string;
    vmescript_command * _next;

  uint16_t _line_number;
};

#endif
