#!/bin/bash

# set -e

ret=0

unwanted="(mvlc_trigger_io-Default_Trigger_IO.vmescript|mvme/templates/auxiliary_scripts/mvlc-soft_trigger.vmescript|mvme/templates/multicrate/mvlc_trigger_io.vmescript|mvme/templates/triva7_master/vme/readout.vmescript|mvme/templates/zzz.caen_v1190a/vme/init-00-Module_Init.vmescript|mvme/templates/tgv/vme/readout.vmescript|mvme/templates/vmmr_monitor/vme/reset.vmescript|mvme/templates/zzz.caen_v1190a/vme/readout.vmescript|mvme/templates/zzz.caen_v785/vme/readout.vmescript|mvme/templates/zzz.caen_v830/vme/readout.vmescript)"

for i in `find mvme/ -name "*.vmescript" | xargs dirname | sort -u`
do
    echo "x "$i
    files=`ls $i/*.vmescript | sort -d | grep -v -E $unwanted`
    # -d = consider only blanks and alphanumeric characters
    # otherwise the order may depend on the locale
    if [[ x$files == x ]]
    then
	echo empty
	
    elif $(./test1  $files > test2.out 2>test2.err)
    then
	#./test1 < test2.out > test2.out2
	true
    else
	echo '---'
	echo $files
	echo " "
	cat test2.err
	echo " "
	echo '---'
	ret=1
    fi
    echo "almost done"
done

echo done
exit $ret
