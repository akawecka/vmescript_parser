all: test

test1: test1.o vmescript_lexer.o vmescript_parser.o vmescript_util.o

test1.o: test1.c vmescript_parser.h

vmescript_util.o: vmescript_util.c vmescript_util.h

vmescript_lexer.c: vmescript_lexer.l
	flex -o $@ $<

vmescript_parser.h: vmescript_parser.c

vmescript_parser.c: vmescript_parser.y
	bison $< --output=$@ --defines=vmescript_parser.h -Wcounterexamples

LDLIBS += -lm
CFLAGS += -g
LDFLAGS += -g

test.stamp: test1 test1.in
	./test1  test1.in > test1.out
	# ./test1 < test1.out > test1.out2
	touch test.stamp

.PHONY: test
test: test.stamp 

clean:
	rm -f *.o vmescript_parser.h vmescript_parser.c vmescript_lexer.c 
